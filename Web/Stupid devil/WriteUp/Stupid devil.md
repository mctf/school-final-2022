# Writeup "Stupid devil"

1) We go to the site, and we see a form here that you can fill out in order to sell your soul for gold. In addition, there are a lot of funny links, I tried :D

![image](./images/Screenshot_1.png)

2) Enter your data and click the "Continue to checkout" button. And we are transferred to a page with a button, after pressing which we will be transferred to ....

![image](./images/Screenshot_2.png)

3) To the page where we will be thanked for the soul, BUT WITHOUT FLAG>:(
Maybe we did something wrong?

![image](./images/Screenshot_3.png)

4) Let's take a look at Burp.

Oops, it turns out we have cookies that were assigned to us after filling out that initial form. If you select sinner cookies, then the inspector will decode the data for us, and we will see our serialized data. In fact, there is the same thing that we entered, except for 1 field. Namely 'devil': 'no'.

![image](./images/Screenshot_4.png)

5) Let's change from 'no' to 'yes', and apply the changes so that burp will encode everything properly. And press the 'Forward' button.

![image](./images/Screenshot_5.png)

6) And we are thrown to the page with the flag. Stupid devil :D
![image](./images/Screenshot_6.png)

