# save this as app.py
from flask import Flask, render_template, request, session
import os, secrets, string
import random

app = Flask(__name__)
secret = secrets.token_urlsafe(16)
app.config['SECRET_KEY'] = secret 

def newCookie():
    cookie = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(25))
    session['data'] = cookie
    print(session['data'])

#Home page
@app.route("/")
def hello():
	if not session.get("data"):
		newCookie()
	return render_template("index.html")


#Form data accepting and creating serialized cookie encoded base 64 and redirecting to page with sacrifice button
@app.route("/calculate", methods=["POST"])
def send():
	if not session.get("data"):
		newCookie()
	firstNumber = str(request.form['firstNumber'])
	secondNumber = str(request.form['secondNumber'])
	summa = str(os.popen('python calculator.py ' + firstNumber + ' ' + secondNumber).read())
	return render_template('return.html', SUM=summa)



#app config
if __name__ == "__main__":
    app.run(debug=False, host=os.environ['IP'], port=os.environ['PORT'])
