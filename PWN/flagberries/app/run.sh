#!/bin/bash

# Check conditions 
if [[ -z $1 ]]; then 
    echo "Specify port plz, closing..."
    exit
elif [[ -z $FLAG ]]; then
    echo 'Check env if FLAG exists, closing...'
    exit
fi 



#Choose what to do before boot (either we on host or docker)
if [[ "$(whoami)" =~ $BIN_NAME ]]; then
    echo "Working in docker, check if binary exists"
    test -f "./$BIN_NAME"
    if [[ $? -eq 0 ]]; then
        echo "$FLAG" > /home/$BIN_NAME/flag.txt
        echo "flag.txt created"
    fi
else
    echo "Working in host..."
    if [[ "$2" =~ "-c" ]]; then
        echo "Compiling..."
        gcc -m32 -O0 -w -no-pie -o $BIN_NAME coffee.c
    fi 
    chmod +x ./$BIN_NAME
fi


if [[ $? -eq 0 ]]; then
    echo "File is ok. start serving localhost:$1 in $(pwd)/$BIN_NAME"

    # socat -s -v tcp-l:$1,reuseaddr,fork "system:./$BIN_NAME 2>&1"
    socat TCP4-LISTEN:$1,reuseaddr,fork,keepalive SYSTEM:"./$BIN_NAME 2>&1",pty,echo=0,end-close,rawer,stderr


    echo "\nExited after serving on port $1"
else
    echo "Failed compiler or file does not exists"
fi

