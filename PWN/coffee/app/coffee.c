#include <stdlib.h>
/* #include <unistd.h> */
#include <stdio.h>
/* #include <string.h> */

int debug_flag;

int main(int argc, char **argv)
{
    char string[512];
    int flag = 1;
    while (flag){
        flag = fgets(string, sizeof(string), stdin);

        printf(string);

        if(debug_flag) {
            printf("debug_flag=%x,turn debugging on...\nDEBUG: %s\n",debug_flag, getenv("FLAG"));
            return 0;
        }
    }
}
