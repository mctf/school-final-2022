$(document).ready(function () {
    var btnSendWords = function () {
        var answ = "";
        $("input[type='text']").each(function () {
            var $this = $(this);
            
            var char = $this.val()
            if(char != ""){
                var symbs = char
                answ += symbs
            }
        });
        // Send POST request to server
        $.ajax({
            type: 'POST',
            url: '/answ',
            data: JSON.stringify(answ),
            success: function(data) {
                if (data.result == "error_uncorrect_words"){
                    alert("Oooops, something is wrong!");
                }else{
                    alert('CONGRATULAITON!\n' + data.result); 
                }
            },
            contentType: "application/json",
            dataType: 'json'

        });




    };
    $("#btnSendWords").click(function () { btnSendWords(); });
});
