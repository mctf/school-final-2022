from flask import Flask, render_template
from flask import request
from flask import jsonify
import os

app = Flask(__name__)



FLAG = os.getenv("FLAG")
checking = 'xfsudosrpkrabackdoorsiemoesqemmetasploitwbhiaizedburpsuitelalepltasbruteforceachookanmapdarbnetcatrkch'


@app.route('/', methods = ['GET', 'POST'])
def index():
    return render_template('index.html')


@app.route('/answ', methods = ['POST'])
def answ():
    content = request.get_json()
    print("DEBUG info:", content)
    if (content.strip().lower() == checking):
        flag = {"result": FLAG}
    else:
        flag = {"result": "error_uncorrect_words"}
    return jsonify(flag)
